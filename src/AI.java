
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class AI {


    /**
     * function alphabeta(node, depth, α, β, maximizingPlayer) is
     *     if depth = 0 or node is a terminal node then
     *         return the heuristic value of node
     *     if maximizingPlayer then
     *         value := −∞
     *         for each child of node do
     *             value := max(value, alphabeta(child, depth − 1, α, β, FALSE))
     *             α := max(α, value)
     *             if α ≥ β then
     *                 break (* β cut-off *)
     *         return value
     *     else
     *         value := +∞
     *         for each child of node do
     *             value := min(value, alphabeta(child, depth − 1, α, β, TRUE))
     *             β := min(β, value)
     *             if α ≥ β then
     *                 break (* α cut-off *)
     *         return value
     */

    private HashMap<String, Node> cacheMaximizing, cacheMinimizing;
    private Creep creep;
    private int c;

    public void init() {
        cacheMaximizing = new HashMap<>();
        cacheMinimizing = new HashMap<>();
        creep = new Creep();
    }

    public Integer[] chooseMove (Board board, int player) {
        ArrayList<Integer[]> moves = allMoves(board);
        int bestScore = player == 1 ? -3 : 3;
        Integer[] bestMove = new Integer[0];
        for (Integer[] move : moves) {
            Board dirty = new Board(board);
            creep.reset();
            for (int i : move) {
                dirty.move(player, i);
                creep.place(i);
            }
            dirty.giveMovePoints(creep.count(dirty), player == 1 ? 2: 1);
            int value = alphabeta(dirty, 0, -3, 3, player == 1 ? 2 : 1, 2);


            System.out.println(String.format("%s/%d value %d", move[0], moves.size(), value));
            if (value == (player == 1 ? 2 : -2)) {
                return move;
            } else {
                if (player == 1) {
                    if (value > bestScore) {
                        bestMove = move;
                    }
                    bestScore = max(bestScore, value);

                } else {
                    if (value < bestScore) {
                        bestMove = move;
                    }
                    bestScore = min(bestScore, value);
                }
            }


        }
        return bestMove;
    }

    public int alphabeta(Board board, int depth, int a, int b, int player, int maximisingPlayer) {
        if (board.hasWinner()) {
            return board.score();
        }
        if (player != maximisingPlayer) { //Maximizing player
            if (cacheMaximizing.containsKey(board.uniqueCode())) {
                return cacheMaximizing.get(board.uniqueCode()).value;
            }
            ArrayList<Integer[]> moves = allMoves(board);
            int value = -3;
            for (Integer[] move : moves) {
                Board dirty = new Board(board);
                creep.reset();
                for (int i : move) {
                    dirty.move(player, i);
                    creep.place(i);
                }
                dirty.giveMovePoints(creep.count(dirty), player == 1 ? 2: 1);
                value = max(value, alphabeta(dirty, depth+1, a, b, player == 1 ? 2 : 1, maximisingPlayer));
                a = max(a, value);
                if (a >= b) {
                    break;
                }
            }

            cacheMaximizing.put(board.uniqueCode(), new Node(board, value));
            return value;
        } else { // Minimizing player
            if (cacheMinimizing.containsKey(board.uniqueCode())) {
                return cacheMinimizing.get(board.uniqueCode()).value;
            }
            ArrayList<Integer[]> moves = allMoves(board);
            int value = 3;
            for (Integer[] move : moves) {
                Board dirty = new Board(board);
                creep.reset();
                for (int i : move) {
                    dirty.move(player, i);
                    creep.place(i);
                }
                dirty.giveMovePoints(creep.count(dirty), player == 1 ? 2: 1);
                value = min(value, alphabeta(dirty, depth+1, a, b, player == 1 ? 2 : 1, maximisingPlayer));
                b = min(b, value);
                if (a >= b) {
                    break;
                }
            }
            cacheMinimizing.put(board.uniqueCode(), new Node(board, value));
            return value;
        }
    }

    public ArrayList<Integer[]> allMoves(Board board) {
        ArrayList<Integer> availablePlaces = board.freePlaces();
        ArrayList<Integer[]> moves = new ArrayList<>();
        return appendMove(board, board.getMoveCount(), availablePlaces,-1, 0, new Integer[board.getMoveCount()], moves);
    }

    private ArrayList<Integer[]> appendMove(Board board, int moveCount, ArrayList<Integer> availablePlaces, int lowest, int depth, Integer[] parent, ArrayList<Integer[]> passAround) {
        for (int place : availablePlaces) {
            if (place <= lowest) {
                continue;
            }
            Integer[] item = parent.clone();
            item[depth] = place;
            if (depth+1 == moveCount) {
                passAround.add(item);
            } else {
                passAround = appendMove(board, moveCount, availablePlaces, place, depth+1, item, passAround);
            }
        }
        return passAround;
    }
}


class Node {
    public String hash;
    public int value = 0;

    public Node(Board board, int value) {
        this.hash = board.uniqueCode();
        this.value = value;
    }
}