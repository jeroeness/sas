import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Creep {

    private Set<Integer> newPlaced;
    public Creep() {
        newPlaced = new HashSet<>();
    }

    public void place(int location) {
        newPlaced.add(location);
    }

    public int count(Board board) {
        HashSet<Integer> checked = new HashSet<>(10);
        HashSet<Integer> discovered = new HashSet<>(10);
        HashSet<Integer> checkList = new HashSet<>(10);
        HashSet<Integer> finalDiscovered = new HashSet<>(10);
        checkList.addAll(newPlaced);
        while (!checkList.isEmpty()) {
            for (int idx : checkList) {
                checked.add(idx);
                discovered.addAll(board.findAdjacent(idx));
            }
            checkList.clear();
            finalDiscovered.addAll(discovered);
            discovered.removeAll(checked);
            checkList.addAll(discovered);

        }
        return Math.max(1, finalDiscovered.size());
    }

    public void reset() {
        newPlaced = new HashSet<>();
    }

    public void reset(int size) {
        newPlaced = new HashSet<>(size);
    }
}
