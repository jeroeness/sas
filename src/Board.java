import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Board {
    public int[] raw;
    private final int[][] adjacencies = {
            {1,4,5},
            {0,2,4,5,6},
            {1,3,5,6,7},
            {2,6,7},
            {0,1,5,8,9},
            {0,1,2,4,6,8,9,10},
            {1,2,3,5,7,9,10,11},
            {2,3,6,10,11},
            {4,5,9,12,13},
            {4,5,6,8,10,12,13,14},
            {5,6,7,9,11,13,14,15},
            {6,7,10,14,15},
            {8,9,13},
            {8,9,10,12,14},
            {9,10,11,13,15},
            {10,11,14}
    };

    public Board(Board board) {
        raw = board.raw.clone();
    }

    private Board() {

    }

    public static Board create() {
        Board b = new Board();
        b.raw = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0};
        return b;
    }

    public void display() {
        System.out.println("   A B C D");
        for (int i =0; i < 4; i++) {
            System.out.println(String.format("%d  %s %s %s %s", i+1, getSymbol(i, 0),getSymbol(i, 1),getSymbol(i, 2), getSymbol(i, 3)));
        }
        System.out.println(String.format("%d", hashCode()));
    }

    public int getIndex(int row, int col) {
        return row*4 + col;
    }

    public int getIndex(String location) {
        try {
            String[] tokens = location.toLowerCase().split("");
            int c = 0;
            switch (tokens[0]) {
                case "b":
                    c = 1;
                    break;
                case "c":
                    c = 2;
                    break;
                case "d":
                    c = 3;
                    break;
            }
            return c + (Integer.parseInt(tokens[1]) - 1) * 4;
        } catch (Exception ex) {
            return -1;
        }

    }

    public String getSymbol(int row, int col) {
        int idx = getIndex(row, col);
        if (raw[idx] == 0){
            return ".";
        } else if (raw[idx] == 1) {
            return "x";
        }
        return "o";
    }

    public ArrayList<Integer> freePlaces() {
        ArrayList<Integer> places = new ArrayList<>(16);
        for (int i =0 ; i < 16; i++) {
            if (raw[i] == 0) {
                places.add(i);
            }
        }
        return places;
    }

    public int winner() {
        for (int i = 0; i < 4 ; i++) {
            int offset = i;
            if (raw[0+offset*4]==raw[1+offset*4] && raw[1+offset*4]==raw[2+offset*4] && raw[2+offset*4] == raw[3+offset*4]) return raw[offset*4];
            if (raw[0+offset]==raw[4+offset] && raw[4+offset]==raw[8+offset] && raw[8+offset] == raw[12+offset]) return raw[offset];
        }
        if (raw[0]==raw[5] && raw[5] == raw[10] && raw[10]==raw[15]) return raw[0];
        if (raw[3]==raw[6] && raw[6] == raw[9] && raw[9]==raw[12]) return raw[3];
        return 0;
    }

    public int mostItems() {
        if (freePlaces().isEmpty()) {
            int count = 0;
            for (int i =0; i < 16; i++) {
                if (raw[i]==1) {
                    count++;
                    if (count > 8) {
                        return 1;
                    }
                }
            }
            if (count < 8) {
                return 2;
            }
        }
        return 0;
    }

    public int score() {
        int w = winner();
        if (w == 0 && freePlaces().isEmpty()) {
            w = mostItems();
            return w == 1 ? 1 : w == 2 ? -1 : 0;
        }
        return w == 1 ? 2 : w == 2 ? -2 : 0;
    }

    public boolean hasWinner() {
        return winner() != 0 || freePlaces().isEmpty();
    }

    public void move (int player, String location) {
        int idx = getIndex(location);
        move(player, idx);
    }

    public void move (int player, int idx) {
        raw[idx] = player;
        raw[15+player]--;

    }

    public boolean legalMove(int idx) {
        return isInside(idx) && raw[idx] == 0;
    }

    public boolean isInside(int idx) {
        return idx>=0 && idx <= 15;
    }

//    @Override
//    public int hashCode() {
//        int[] r = rotate(raw);
//        int hash = Arrays.hashCode(raw) + Arrays.hashCode(r);
//        for (int i =0; i < 2; i++) {
//            r = rotate(r);
//            hash += Arrays.hashCode(r);
//        }
//        r = flip(raw);
//        hash += Arrays.hashCode(r);
//        for (int i =0; i < 3; i++) {
//            r = rotate(r);
//            hash += Arrays.hashCode(r);
//        }
//
//        return hash;
//    }

    public String uniqueCode() {
        int[] r = rotate(raw);
        String hash = rawHashCode(raw).compareTo(rawHashCode(r)) > 0 ? rawHashCode(raw) : rawHashCode(r);
        for (int i =0; i < 2; i++) {
            r = rotate(r);
            hash = hash.compareTo(rawHashCode(r)) > 0 ? hash : rawHashCode(r);
        }
        r = flip(raw);
        hash = hash.compareTo(rawHashCode(r)) > 0 ? hash : rawHashCode(r);
        for (int i =0; i < 3; i++) {
            r = rotate(r);
            hash = hash.compareTo(rawHashCode(r)) > 0 ? hash : rawHashCode(r);
        }
        return hash;
    }

    private String rawHashCode(int[] r) {
        return Arrays.toString(r);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null || !(obj instanceof Board))
            return false;
        return hashCode() == obj.hashCode();
    }

    public int availableMoves(int i) {
        return raw[15+i];
    }

    public Set<Integer> findAdjacent(int middleIdx) {
        int key = raw[middleIdx];

        HashSet<Integer> discovered = new HashSet<>(6);
        for (int p : adjacencies[middleIdx]) {
            if (raw[p] == key) {
                discovered.add(p);
            }
        }
        return discovered;
    }

    public void giveMovePoints(int movePoints, int player) {
        raw[15+player] =  Math.min(movePoints, freePlaces().size());
    }

    private int[] rotate(int[] r) {
        return new int[] {r[12],r[8],r[4],r[0],r[13],r[9],r[5],r[1],r[14],r[10],r[6],r[2],r[15],r[11],r[7],r[3],r[16],r[17]};
    }

    private int[] flip(int[] r) {
        return new int[] {r[3],r[2],r[1],r[0],r[7],r[6],r[5],r[4],r[11],r[10],r[9],r[8],r[15],r[14],r[13],r[12],r[16],r[17]};
    }

    public int getMoveCount() {
        return raw[16]+raw[17];
    }
}

