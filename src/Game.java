import java.io.*;
import java.util.Scanner;

public class Game {

    private Board board;
    private Creep creep;
    public void start() {
        board = Board.create();
        creep = new Creep();
        while (!board.hasWinner()) {
            while (board.availableMoves(1) > 0) {
                board.display();

                if (board.hasWinner()) {
                    break;
                }

                userInput(1, board.availableMoves(1));
            }
            int movePoints = creep.count(board);
            creep.reset();
            board.giveMovePoints(movePoints, 2);
            while (board.availableMoves(2) > 0) {
                board.display();

                if (board.hasWinner()) {
                    break;
                }

                userInput(2, board.availableMoves(2));
            }
            movePoints = creep.count(board);
            creep.reset();
            board.giveMovePoints(movePoints, 1);
        }
        System.out.println(String.format("WINNER player %d", board.winner()));
    }

    public void startVsAi() {
        board = Board.create();
        creep = new Creep();
        AI ai = new AI();
        ai.init();
//        board.move(2, 0);
//        board.giveMovePoints(1, 1);
        while (!board.hasWinner()) {
            while (board.availableMoves(1) > 0) {
                board.display();

                if (board.hasWinner()) {
                    break;
                }

                userInput(1, board.availableMoves(1));
            }
            if (board.hasWinner()) {
                break;
            }
            int movePoints = creep.count(board);
            creep.reset();
            board.giveMovePoints(movePoints, 2);
            Integer[] AIMoves = ai.chooseMove(board, 2);
            for (int i : AIMoves) {
                board.display();

                if (board.hasWinner()) {
                    break;
                }

                board.move(2, i);
                creep.place(i);
            }
            movePoints = creep.count(board);
            creep.reset();
            board.giveMovePoints(movePoints, 1);
        }
        board.display();
        if (board.winner() == 0 && board.mostItems() == 0) {
            System.out.println("Draw!");
        } else if (board.winner() == 0) {
            System.out.println(String.format("Majority winner player %d", board.mostItems()));
        } else {
            System.out.println(String.format("WINNER player %d", board.winner()));
        }
    }

    private void userInput(int player, int moves) {
        System.out.print(String.format("\n Player %d (%d)> ", player, moves));
        Scanner stdin = new Scanner(System.in);
        String line = stdin.nextLine();
        if (board.legalMove(board.getIndex(line))) {
            board.move(player, line);
            creep.place(board.getIndex(line));
        } else {
            System.out.println("\nIllegal move!");
            userInput(player, moves);
        }
        System.out.println("");

    }

    public void AiVsAi() {
        board = Board.create();
        creep = new Creep();
        AI ai = new AI();
        AI ai2 = new AI();
        ai.init();
        ai2.init();
        while (!board.hasWinner()) {
            Integer[] AIMoves = ai.chooseMove(board, 1);
            for (int i : AIMoves) {

                if (board.hasWinner()) {
                    break;
                }

                board.move(1, i);
                board.display();
                creep.place(i);
            }
            int movePoints = creep.count(board);
            creep.reset();
            board.giveMovePoints(movePoints, 2);
            AIMoves = ai2.chooseMove(board, 2);
            for (int i : AIMoves) {

                if (board.hasWinner()) {
                    break;
                }

                board.move(2, i);
                board.display();
                creep.place(i);
            }
            movePoints = creep.count(board);
            creep.reset();
            board.giveMovePoints(movePoints, 1);
        }
        board.display();
        if (board.winner() == 0 && board.mostItems() == 0) {
            System.out.println("Draw!");
        } else if (board.winner() == 0) {
            System.out.println(String.format("Majority winner player %d", board.mostItems()));
        } else {
            System.out.println(String.format("WINNER player %d", board.winner()));
        }
    }
}
