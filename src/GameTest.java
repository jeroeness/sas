import static org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class GameTest {

    private Board board;
    private Creep creep;

    @org.junit.Test
    public void start() {
        board = Board.create();
        creep = new Creep();
        moveTest(1, "a1",1);
        moveTest(2, "d4",1);
        moveTest(1, "c4",1);
        moveTest(2, "b2",1);
        moveTest(1, "d2",1);
        moveTest(2, "d1",1);
        moveTest(1, "a3",1);
        moveTest(2, "d4",1);
        moveTest(1, "b3",3);
    }

    private void moveTest(int player, String move, int creepCount) {
        board.move(player, move);
        creep.place(board.getIndex(move));
        int movePoints = creep.count(board);
        assertEquals(creepCount, movePoints);
        creep.reset();
        board.giveMovePoints(movePoints, player == 1 ? 2 : 1);
    }

    @Test
    public void moves() {
        board = Board.create();
        AI ai = new AI();
        ai.init();
        board.giveMovePoints(3,1);
        ArrayList<Integer[]> moves = ai.allMoves(board);
        assertEquals(moves.size(), 560);

    }

    @Test
    public void hashTest() {
        Board b1 = Board.create();
        b1.move(1,0);
        b1.move(2,4);
        b1.move(1,5);

        Board b2 = Board.create();
        b2.move(1,15);
        b2.move(2,14);
        b2.move(1,10);
        assertTrue(b1.equals(b2));

    }

    @Test
    public void aiTest() {
        board = Board.create();
        board.move(1, 12);
        board.move(1, 4);
        board.move(1, 6);
        board.move(1, 14);
        board.move(2, 1);
        board.move(2, 3);
        board.move(2, 9);
        board.giveMovePoints(0,1);
        board.giveMovePoints(1,2);
        board.display();
        AI ai = new AI();
        ai.init();
        Integer[] moves = ai.chooseMove(board, 2);
        board.move(2, moves[0]);
        board.display();
    }
}